# R8125_VERSION = 10846ccfd2a56d575dfc01c15dbf1f1e809da212
R8125_SITE = https://gitlab.com/toshikidev/shikiology/shiki-v1/shiki-r8125
R8125_LICENSE = GPL-2.0

$(eval $(kernel-module))
$(eval $(generic-package))

