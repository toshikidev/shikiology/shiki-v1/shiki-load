# R8168_VERSION = 52c98bd764e6dd22ff17876afa655e9e11237cc9
R8168_SITE = https://gitlab.com/toshikidev/shikiology/shiki-v1/shiki-r8168
R8168_LICENSE = GPL-2.0

$(eval $(kernel-module))
$(eval $(generic-package))
